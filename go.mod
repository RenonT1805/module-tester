module module-tester

go 1.16

require (
	github.com/Microsoft/go-winio v0.5.0
	github.com/mattn/go-colorable v0.1.8
	github.com/mitchellh/go-ps v1.0.0
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/text v0.3.6
)
