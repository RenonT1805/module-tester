package stubs

import (
	"context"
	"errors"
	"io/ioutil"
	"module-tester/commons"
	"module-tester/testing/evaluators"
	"module-tester/testing/parameters"
	"module-tester/testing/repository"

	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

// httpStub : HTTPサーバーをホストするスタブ
type httpStub struct {
	Response   httpResponseOption
	Condition  *httpCondition
	Path       string
	Timeout    int
	Repository repository.Repository
}

// httpResponseOption : HTTPレスポンスのオプション
type httpResponseOption struct {
	StatusCode int             `json:"StatusCode"`
	Header     http.Header     `json:"Header"`
	Body       parameters.Body `json:"Body"`
}

// httpCondition : HTTPリクエストの判定用データ
type httpCondition struct {
	SubPath string          `json:"SubPath"`
	Method  string          `json:"Method"`
	Header  http.Header     `json:"Header"`
	Body    parameters.Body `json:"Body"`
}

type httpServerHandler struct {
	Handler func(w http.ResponseWriter, r *http.Request)
}

func (h *httpServerHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.Handler(w, r)
}

// Listen : HTTPサーバーを立ち上げる
func (s httpStub) Listen() ProcessResult {
	resultChan := make(chan ProcessResult, 1)
	closeSignal := make(chan bool, 1)
	timeoutFlag := true
	srv := &http.Server{
		Addr: s.Path,
		Handler: &httpServerHandler{
			Handler: func(w http.ResponseWriter, r *http.Request) {
				result := ProcessResult{
					Evaluator: nil,
					Response:  s.Response,
					Begin:     time.Now(),
				}
				content, err := ioutil.ReadAll(r.Body)
				if err != nil {
					result.Evaluator = evaluators.CreateFailedEvaluator(err)
					resultChan <- result
					return
				}

				var requestBody parameters.Body
				if s.Condition != nil {
					requestBody = parameters.CreateBodyFromBytes(
						content, s.Condition.Body.CharacterCode, s.Condition.Body.Format,
					)
				} else {
					requestBody.Data = string(content)
				}
				result.Request = evaluators.HTTPRequestCondition{
					Body:    requestBody,
					Header:  r.Header,
					Method:  r.Method,
					SubPath: r.URL.Path,
				}

				for key, header := range s.Response.Header {
					for _, item := range header {
						w.Header().Add(key, item)
					}
				}
				if s.Response.StatusCode != 0 {
					w.WriteHeader(s.Response.StatusCode)
				}
				w.Write(s.Response.Body.DataToBytes())

				result.End = time.Now()
				resultChan <- result
				closeSignal <- true
				timeoutFlag = false
			},
		},
	}

	go func() {
		time.Sleep(time.Second * time.Duration(s.Timeout))
		if timeoutFlag && s.Timeout != 0 {
			closeSignal <- true
			resultChan <- ProcessResult{
				Evaluator: evaluators.CreateFailedEvaluator(errors.New("受付がタイムアウトしました")),
			}
		}
	}()

	go func() {
		<-closeSignal
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			log.Infoln(err)
		}
	}()

	if err := srv.ListenAndServe(); err != nil {
		log.Infoln(err)
	}

	result := <-resultChan
	if result.Evaluator != nil {
		return result
	}

	if s.Condition == nil {
		result.Evaluator = evaluators.CreateCorrectEvaluator()
	} else if request, castable := result.Request.(evaluators.HTTPRequestCondition); castable {
		result.Evaluator = evaluators.CreateHTTPRequestEvaluator(
			request,
			evaluators.HTTPRequestCondition{
				Header:  s.Condition.Header,
				Method:  s.Condition.Method,
				SubPath: s.Condition.SubPath,
				Body:    s.Condition.Body,
			},
			s.Repository,
		)
	} else {
		result.Evaluator = evaluators.CreateFailedEvaluator(errors.New("Evaluatorの作成に失敗しました"))
	}
	return result
}

// CreateHTTPStub : HTTPサーバーをホストするスタブを生成する
func CreateHTTPStub(option StubOption, repository repository.Repository) (Stub, error) {
	var response httpResponseOption
	err := commons.ToStruct(option.Response, &response)
	if err != nil {
		return failedStub{
			Err: err,
		}, err
	}
	response.Body.SetFromRepository(repository)

	var condition *httpCondition
	if option.Condition != nil {
		var c httpCondition
		condition = &c
		err = commons.ToStruct(option.Condition, condition)
		if err != nil {
			return failedStub{
				Err: err,
			}, err
		}
	}

	return httpStub{
		Condition:  condition,
		Response:   response,
		Path:       option.Path,
		Timeout:    option.TimeoutSeconds,
		Repository: repository,
	}, nil
}
