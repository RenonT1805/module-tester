package stubs

import (
	"errors"
	"module-tester/commons"
	"module-tester/testing/evaluators"
	"module-tester/testing/parameters"
	"module-tester/testing/repository"

	"net"
	"time"

	"github.com/Microsoft/go-winio"
)

// namedPipeStub : 名前付きパイプサーバーをホストするスタブ
type namedPipeStub struct {
	Response   namedPipeServerOption
	Condition  *parameters.Body
	Path       string
	Timeout    int
	Repository repository.Repository
}

// namedPipeServerOption : 名前付きパイプサーバーのオプション
type namedPipeServerOption struct {
	InputBufferSize  int32           `json:"InputBufferSize"`
	OutputBufferSize int32           `json:"OutputBufferSize"`
	Body             parameters.Body `json:"Body"`
}

// communicationFromNamedPipeServer : 名前付きパイプサーバーによってリクエストレスポンスのやり取りを行う
func (s namedPipeStub) communicationFromNamedPipeServer(listener net.Listener, conn net.Conn) ProcessResult {
	result := ProcessResult{
		Response: s.Response,
		Begin:    time.Now(),
	}
	data := make([]byte, s.Response.InputBufferSize)
	l, err := conn.Read(data)
	if err != nil {
		return returnsFailedStubResult(result, err)
	}
	data = data[0:l]

	_, err = conn.Write(s.Response.Body.DataToBytes())
	if err != nil {
		return returnsFailedStubResult(result, err)
	}
	result.End = time.Now()

	var requestBody parameters.Body
	if s.Condition != nil {
		requestBody = parameters.CreateBodyFromBytes(
			data, s.Condition.CharacterCode, s.Condition.Format,
		)
	} else {
		requestBody.Data = string(data)
	}

	result.Request = requestBody
	if s.Condition != nil {
		result.Evaluator = evaluators.CreateDataEvaluator(
			requestBody,
			*s.Condition,
			s.Repository,
		)
	} else {
		result.Evaluator = evaluators.CreateCorrectEvaluator()
	}
	return result
}

// Listen : 名前付きパイプサーバーを立ち上げ、結果を返却する
func (s namedPipeStub) Listen() ProcessResult {
	pipeConfig := winio.PipeConfig{
		SecurityDescriptor: "S:(ML;;NW;;;LW)D:(A;;0x12019f;;;WD)",
		InputBufferSize:    s.Response.InputBufferSize,
		OutputBufferSize:   s.Response.OutputBufferSize,
	}

	listener, err := winio.ListenPipe(`\\.\pipe\`+s.Path, &pipeConfig)
	if err != nil {
		return returnsFailedStubResult(ProcessResult{
			Response: s.Response,
		}, err)
	}
	defer listener.Close()

	conn, err := listener.Accept()
	if err != nil {
		return returnsFailedStubResult(ProcessResult{
			Response: s.Response,
		}, err)
	}
	defer conn.Close()

	timeoutFlag := true
	resultChan := make(chan ProcessResult, 1)
	go func(listener net.Listener, conn net.Conn) {
		resultChan <- s.communicationFromNamedPipeServer(listener, conn)
		timeoutFlag = false
	}(listener, conn)

	go func() {
		time.Sleep(time.Second * time.Duration(s.Timeout))
		if timeoutFlag && s.Timeout != 0 {
			resultChan <- ProcessResult{
				Response:  s.Response,
				Evaluator: evaluators.CreateFailedEvaluator(errors.New("受付がタイムアウトしました")),
			}
		}
	}()

	result := <-resultChan
	return result
}

// CreatenamedPipeStub : 名前付きパイプサーバーをホストするスタブを生成する
func CreatenamedPipeStub(option StubOption, repository repository.Repository) (Stub, error) {
	var response namedPipeServerOption
	err := commons.ToStruct(option.Response, &response)
	if err != nil {
		return failedStub{
			Err: err,
		}, err
	}
	response.Body.SetFromRepository(repository)

	var condition *parameters.Body
	if option.Condition != nil {
		var c parameters.Body
		condition = &c
		err = commons.ToStruct(option.Condition, condition)
		if err != nil {
			return failedStub{
				Err: err,
			}, err
		}
	}

	return namedPipeStub{
		Response:   response,
		Condition:  condition,
		Path:       option.Path,
		Repository: repository,
		Timeout:    option.TimeoutSeconds,
	}, nil
}
