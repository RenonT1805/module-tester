package drivers

import (
	"bytes"
	"io/ioutil"
	"module-tester/commons"
	"module-tester/testing/evaluators"
	"module-tester/testing/parameters"
	"module-tester/testing/repository"

	"net/http"
	"strings"
	"time"
)

// httpDriver : HTTPリクエストを行うオブジェクト
type httpDriver struct {
	Request    httpRequestOption
	Condition  *httpCondition
	Path       string
	Repository repository.Repository
}

// httpRequestOption : HTTPリクエストのオプション
type httpRequestOption struct {
	SubPath string          `json:"SubPath"`
	Method  string          `json:"Method"`
	Header  http.Header     `json:"Header"`
	Body    parameters.Body `json:"Body"`
}

// httpCondition : HTTPレスポンスの判定用データ
type httpCondition struct {
	StatusCode int             `json:"StatusCode"`
	Header     http.Header     `json:"Header"`
	Body       parameters.Body `json:"Body"`
}

func httpRequest(method, path string, data []byte, header http.Header) (*http.Response, error) {
	request, err := http.NewRequest(method, path, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	if header != nil {
		request.Header = header
	}

	client := http.Client{}
	return client.Do(request)
}

// Drive : HTTPリクエストを実行し、結果を返却する
func (d httpDriver) Drive() DriveResult {
	result := DriveResult{}
	if !strings.HasPrefix(d.Request.SubPath, "/") {
		d.Request.SubPath += "/"
	}

	result.Begin = time.Now()
	response, err := httpRequest(
		d.Request.Method,
		d.Path,
		d.Request.Body.DataToBytes(),
		d.Request.Header,
	)
	if err != nil {
		return returnsFailedDriveResult(result, err)
	}
	defer response.Body.Close()
	result.End = time.Now()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return returnsFailedDriveResult(result, err)
	}

	var responseBody parameters.Body
	if d.Condition != nil {
		responseBody = parameters.CreateBodyFromBytes(
			data, d.Condition.Body.CharacterCode, d.Condition.Body.Format,
		)
	} else {
		responseBody.Data = string(data)
	}

	result.Request = d.Request
	result.Response = evaluators.HTTPResponseCondition{
		Header:     response.Header,
		StatusCode: response.StatusCode,
		Body:       responseBody,
	}
	if d.Condition != nil {
		result.Evaluator = evaluators.CreateHTTPResponseEvaluator(
			result.Response.(evaluators.HTTPResponseCondition),
			evaluators.HTTPResponseCondition{
				StatusCode: d.Condition.StatusCode,
				Header:     d.Condition.Header,
				Body:       d.Condition.Body,
			},
			d.Repository,
		)
	} else {
		result.Evaluator = evaluators.CreateCorrectEvaluator()
	}

	return result
}

// CreateHTTPDriver : HTTPリクエストを行うオブジェクトを作成する
func CreateHTTPDriver(option DriverOption, repository repository.Repository) (Driver, error) {
	var request httpRequestOption
	err := commons.ToStruct(option.Request, &request)
	if err != nil {
		return failedDriver{
			Err: err,
		}, err
	}
	request.Body.SetFromRepository(repository)

	var condition *httpCondition
	if option.Condition != nil {
		var c httpCondition
		condition = &c
		err = commons.ToStruct(option.Condition, condition)
		if err != nil {
			return failedDriver{
				Err: err,
			}, err
		}
	}

	if !strings.HasPrefix(request.SubPath, "/") {
		request.SubPath = "/" + request.SubPath
	}

	return httpDriver{
		Request:    request,
		Condition:  condition,
		Path:       option.Path + request.SubPath,
		Repository: repository,
	}, nil
}
