package drivers

import (
	"module-tester/commons"
	"module-tester/testing/evaluators"
	"os/exec"
	"time"
)

// executeDriver : プロセス実行を行うオブジェクト
type executeDriver struct {
	Request   executeRequestOption
	Condition *executeCondition
	Path      string
}

// executeRequestOption : Executeのオプション
type executeRequestOption struct {
	Arguments []string `json:"Arguments"`
}

// executeCondition : Executeの判定用データ
type executeCondition struct {
	SurvivalTimeSeconds int `json:"SurvivalTimeSeconds"`
}

// Drive : プロセスを実行し、結果を返却する
func (d executeDriver) Drive() DriveResult {
	result := DriveResult{
		Request: d.Request,
		Begin:   time.Now(),
	}

	cmd := exec.Command(d.Path, d.Request.Arguments...)
	err := cmd.Start()
	if err != nil {
		result.Evaluator = evaluators.CreateFailedEvaluator(err)
	} else if d.Condition == nil {
		result.Evaluator = evaluators.CreateCorrectEvaluator()
	} else {
		result.Evaluator = evaluators.CreateProcessEvaluator(cmd, d.Condition.SurvivalTimeSeconds)
	}

	result.End = time.Now()
	return result
}

// CreateExecuteDriver : Executeリクエストを行うオブジェクトを作成する
func CreateExecuteDriver(option DriverOption) (Driver, error) {
	var request executeRequestOption
	err := commons.ToStruct(option.Request, &request)
	if err != nil {
		return failedDriver{
			Err: err,
		}, err
	}

	var condition *executeCondition
	if option.Condition != nil {
		var c executeCondition
		condition = &c
		err = commons.ToStruct(option.Condition, condition)
		if err != nil {
			return failedDriver{
				Err: err,
			}, err
		}
	}

	return executeDriver{
		Request:   request,
		Condition: condition,
		Path:      option.Path,
	}, nil
}
