package drivers

import (
	"module-tester/commons"
	"module-tester/testing/evaluators"
	"module-tester/testing/parameters"
	"module-tester/testing/repository"

	"net"
	"time"

	"github.com/Microsoft/go-winio"
)

// namedPipeDriver : 名前付きパイプによるリクエストを行うオブジェクト
type namedPipeDriver struct {
	Request    namedPipeClientOption
	Condition  *parameters.Body
	Path       string
	Repository repository.Repository
}

// namedPipeClientOption : 名前付きパイプクライアントのオプション
type namedPipeClientOption struct {
	TimeoutSeconds int             `json:"TimeoutSeconds"`
	BufferSize     int             `json:"BufferSize"`
	Body           parameters.Body `json:"Body"`
}

// Drive : 名前付きパイプによるリクエストを行う
func (d namedPipeDriver) Drive() DriveResult {
	result := DriveResult{}
	var err error
	var conn net.Conn

	result.Request = d.Request.Body
	result.Begin = time.Now()
	var timeout *time.Duration
	if d.Request.TimeoutSeconds != 0 {
		t := time.Second * time.Duration(d.Request.TimeoutSeconds)
		timeout = &t
	}
	for {
		conn, err = winio.DialPipe(`\\.\pipe\`+d.Path, timeout)
		if err != nil {
			if timeout != nil && time.Now().Unix() < result.Begin.Add(*timeout).Unix() {
				continue
			}
			return returnsFailedDriveResult(result, err)
		}
		break
	}

	defer conn.Close()
	conn.Write(d.Request.Body.DataToBytes())

	data := make([]byte, d.Request.BufferSize)
	l, err := conn.Read(data)
	if err != nil {
		return returnsFailedDriveResult(result, err)
	}
	result.End = time.Now()
	data = data[0:l]

	var responseBody parameters.Body
	if d.Condition != nil {
		responseBody = parameters.CreateBodyFromBytes(
			data, d.Condition.CharacterCode, d.Condition.Format,
		)
	} else {
		responseBody.Data = string(data)
	}
	result.Response = responseBody

	if d.Condition != nil {
		result.Evaluator = evaluators.CreateDataEvaluator(
			responseBody,
			*d.Condition,
			d.Repository,
		)
	} else {
		result.Evaluator = evaluators.CreateCorrectEvaluator()
	}

	return result
}

// CreateNamedPipeDriver : 名前付きパイプによるリクエストを行うオブジェクトを生成する
func CreateNamedPipeDriver(option DriverOption, repository repository.Repository) (Driver, error) {
	var request namedPipeClientOption
	err := commons.ToStruct(option.Request, &request)
	if err != nil {
		return failedDriver{
			Err: err,
		}, err
	}
	request.Body.SetFromRepository(repository)

	var condition *parameters.Body
	if option.Condition != nil {
		var c parameters.Body
		condition = &c
		err = commons.ToStruct(option.Condition, condition)
		if err != nil {
			return failedDriver{
				Err: err,
			}, err
		}
	}

	return namedPipeDriver{
		Request:    request,
		Condition:  condition,
		Path:       option.Path,
		Repository: repository,
	}, nil
}
