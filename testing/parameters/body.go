package parameters

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"module-tester/constants"
	"module-tester/testing/repository"
	"strings"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/japanese"
	"golang.org/x/text/transform"
)

// Body : DriverやStubでやり取りするデータ
type Body struct {
	CharacterCode string      `json:"CharacterCode"`
	Format        string      `json:"Format"`
	Data          interface{} `json:"Data"`
}

// IsText : データが文字列形式か否か
func (d Body) IsText() bool {
	switch strings.ToUpper(d.Format) {
	case "", "TEXT", "STRING":
		return true
	}
	return false
}

// IsJSON : データがJSON形式か否か
func (d Body) IsJOSN() bool {
	switch strings.ToUpper(d.Format) {
	case "JSON":
		return true
	}
	return false
}

// ToString : データを文字列に変換する
func (d Body) DataToString() string {
	switch {
	case d.IsText():
		if val, castable := d.Data.(string); castable {
			return val
		}
	case d.IsJOSN():
		if val, err := json.Marshal(d.Data); err == nil {
			return string(val)
		}
	}
	return ""
}

// ToString : データを文字列のバイト列に変換する
func (d Body) DataToBytes() []byte {
	encoded, err := encodeFromUTF8([]byte(d.DataToString()), d.CharacterCode)
	if err != nil {
		return []byte(d.DataToString())
	}
	return encoded
}

// SetFromRepository : データがmapオブジェクトや配列オブジェクトのとき、リポジトリに保存した値をセットする
func (d *Body) SetFromRepository(repo repository.Repository) {
	d.Data = dataSetFromRepository(d.Data, repo)
}

func dataSetFromRepository(data interface{}, repo repository.Repository) interface{} {
	var result interface{}
	if m, castable := data.(map[string]interface{}); castable {
		for key := range m {
			m[key] = dataSetFromRepository(m[key], repo)
		}
		result = m
	} else if ary, castable := data.([]interface{}); castable {
		for idx := range ary {
			ary[idx] = dataSetFromRepository(ary[idx], repo)
		}
		result = ary
	} else if v, castable := data.(string); repo != nil && castable &&
		strings.HasPrefix(v, constants.ValiableValuePrefix) && strings.HasSuffix(v, constants.ValiableValueSuffix) {
		v = v[len(constants.ValiableValuePrefix) : len(v)-len(constants.ValiableValueSuffix)]
		if len(v) > 0 {
			repoVal, err := repo.Get(v)
			if err == nil {
				result = repoVal
			}
		}
	} else {
		result = data
	}

	return result
}

func decodeToUTF8(source []byte, characterCode string) ([]byte, error) {
	var decoder *encoding.Decoder
	switch strings.ToUpper(characterCode) {
	case "UTF8", "UTF-8", "":
		return bytes.TrimPrefix(source, []byte("\xef\xbb\xbf")), nil
	case "EUCJP":
		decoder = japanese.EUCJP.NewDecoder()
	case "ISO2022JP":
		decoder = japanese.ISO2022JP.NewDecoder()
	case "SHIFT-JIS", "SHIFTJIS", "SJIS", "S-JIS":
		decoder = japanese.ShiftJIS.NewDecoder()
	default:
		return nil, errors.New("'" + characterCode + "' は存在しないDecoderです")
	}
	reader := transform.NewReader(bytes.NewReader(source), decoder)
	result, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func encodeFromUTF8(source []byte, characterCode string) ([]byte, error) {
	var decoder *encoding.Encoder
	switch strings.ToUpper(characterCode) {
	case "UTF8", "UTF-8", "":
		return bytes.TrimPrefix(source, []byte("\xef\xbb\xbf")), nil
	case "EUCJP":
		decoder = japanese.EUCJP.NewEncoder()
	case "ISO2022JP":
		decoder = japanese.ISO2022JP.NewEncoder()
	case "SHIFT-JIS", "SHIFTJIS", "SJIS", "S-JIS":
		decoder = japanese.ShiftJIS.NewEncoder()
	default:
		return nil, errors.New("'" + characterCode + "' は存在しないDecoderです")
	}
	reader := transform.NewReader(bytes.NewReader(source), decoder)
	result, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// CreateBodyFromBytes : バイトデータからBodyを作成する
func CreateBodyFromBytes(bytes []byte, characterCode, format string) Body {
	result := Body{
		CharacterCode: characterCode,
		Format:        format,
	}
	decoded, err := decodeToUTF8(bytes, characterCode)
	if err != nil {
		result.Data = string(bytes)
		return result
	}

	var responseData interface{}
	switch {
	case result.IsText():
		responseData = string(decoded)
	case result.IsJOSN():
		if err := json.Unmarshal(decoded, &responseData); err != nil {
			responseData = string(decoded)
		}
	default:
		responseData = string(decoded)
	}
	result.Data = responseData
	return result
}
