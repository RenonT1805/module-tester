package testing

import (
	"module-tester/testing/drivers"
	"module-tester/testing/stubs"
)

type ScenarioTest struct {
	MaxParallel int        `json:"MaxParallel"`
	Scenarios   []Scenario `json:"Scenarios"`
}

type Scenario struct {
	Name        string          `json:"Name"`
	Async       bool            `json:"Async"`
	Description string          `json:"Description"`
	Sequence    []ModuleSetting `json:"Sequence"`
}

type ModuleSetting struct {
	Driver *drivers.DriverOption `json:"Driver"`
	Stubs  *[]stubs.StubOption   `json:"Stubs"`
}
