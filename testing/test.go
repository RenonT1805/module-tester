package testing

import (
	"errors"
	"module-tester/testing/drivers"
	"module-tester/testing/repository"
	"module-tester/testing/stubs"

	log "github.com/sirupsen/logrus"
)

type asyncTestResult struct {
	result ScenarioTestResult
	err    error
}

func getTestResultFromDriver(result drivers.DriveResult) TestResult {
	var detail *TestResultDetail
	evaluationResult := result.Evaluator.Evaluation()
	if result.Response != nil {
		detail = &TestResultDetail{
			Begin:    result.Begin,
			End:      result.End,
			Request:  result.Request,
			Response: result.Response,
			Score:    float64(result.End.Sub(result.Begin).Microseconds()) / 1000,
		}
	}
	return TestResult{
		Correct: evaluationResult.Correct,
		Message: evaluationResult.Message,
		Detail:  detail,
	}
}

func getTestResultFromStub(result stubs.ProcessResult) TestResult {
	var detail *TestResultDetail
	evaluationResult := result.Evaluator.Evaluation()
	if result.Request != nil {
		detail = &TestResultDetail{
			Begin:    result.Begin,
			End:      result.End,
			Request:  result.Request,
			Response: result.Response,
			Score:    float64(result.End.Sub(result.Begin).Microseconds()) / 1000,
		}
	}
	return TestResult{
		Correct: evaluationResult.Correct,
		Message: evaluationResult.Message,
		Detail:  detail,
	}
}

func runScenario(scenario Scenario) (ScenarioTestResult, error) {
	log.WithFields(log.Fields{
		"シナリオ名": scenario.Name,
	}).Infoln("テスト開始")

	result := ScenarioTestResult{
		ScenarioName: scenario.Name,
		Results:      nil,
	}

	var err error
	repository := repository.CreateStandardRepository()
	moduleTestResult := make([]ModuleTestResult, len(scenario.Sequence))
	for sequenceNo, moduleTest := range scenario.Sequence {
		var stubsChan chan stubs.ProcessResult
		if moduleTest.Stubs != nil {
			stubsChan = make(chan stubs.ProcessResult, len(*moduleTest.Stubs))
			for stubNo, stubOption := range *moduleTest.Stubs {
				l := log.WithFields(log.Fields{
					"スタブ番号":    stubNo,
					"シークエンス番号": sequenceNo,
				})
				if stubOption.TimeoutSeconds == 0 {
					l.Warnln("タイムアウトが設定されていません")
				}
				stub, err := stubs.CreateStub(stubOption, repository)
				if err != nil {
					log.WithError(err).Errorln("Stubの生成に失敗しました")
					return result, err
				}
				go func(stub stubs.Stub, log *log.Entry) {
					log.Infoln("スタブを開始します")
					stubsChan <- stub.Listen()
					log.Infoln("スタブを終了します")
				}(stub, l)
			}
		}

		var driverResult *TestResult
		if moduleTest.Driver != nil {
			log := log.WithFields(log.Fields{
				"シークエンス番号": sequenceNo,
			})
			driver, err := drivers.CreateDriver(*moduleTest.Driver, &repository)
			if err != nil {
				log.WithError(err).Errorln("Driverの生成に失敗しました")
				return result, err
			}
			log.Infoln("ドライバーを開始します")
			d := getTestResultFromDriver(driver.Drive())
			log.Infoln("ドライバーを終了します")
			driverResult = &d
			if !d.Correct {
				log.Errorln(d.Message)
				err = errors.New("テスト失敗")
			} else {
				log.Infoln(d.Message)
			}
		}

		var stubResult *[]TestResult
		if moduleTest.Stubs != nil {
			s := make([]TestResult, len(*moduleTest.Stubs))
			for i := range *moduleTest.Stubs {
				log := log.WithFields(log.Fields{
					"スタブ番号":    i,
					"シークエンス番号": sequenceNo,
				})
				s[i] = getTestResultFromStub(<-stubsChan)
				if !s[i].Correct {
					log.Errorln(s[i].Message)
					err = errors.New("テスト失敗")
				} else {
					log.Infoln(s[i].Message)
				}
			}
			stubResult = &s
		}

		moduleTestResult[sequenceNo] = ModuleTestResult{
			Driver: driverResult,
			Stubs:  stubResult,
		}
	}

	return ScenarioTestResult{
		ScenarioName: scenario.Name,
		Results:      moduleTestResult,
	}, err
}

func RunScenarioTest(scenarioTest ScenarioTest) ([]ScenarioTestResult, bool) {
	correctFlag := true
	results := []ScenarioTestResult{}

	var asyncCount int
	asyncResult := make(chan asyncTestResult, scenarioTest.MaxParallel)
	for _, scenario := range scenarioTest.Scenarios {
		if scenario.Async && scenarioTest.MaxParallel > 1 && asyncCount < scenarioTest.MaxParallel {
			go func(s Scenario) {
				res, err := runScenario(s)
				asyncResult <- asyncTestResult{
					err:    err,
					result: res,
				}
			}(scenario)
			asyncCount++
			continue
		} else if asyncCount > 0 {
			for ; asyncCount > 0; asyncCount-- {
				res := <-asyncResult
				if res.err != nil {
					log.WithError(res.err).Errorln("テストに失敗しました")
					correctFlag = false
				}
				results = append(results, res.result)
			}
		}

		result, err := runScenario(scenario)
		if err != nil {
			log.WithError(err).Errorln("テストに失敗しました")
			correctFlag = false
		}
		results = append(results, result)
	}

	if asyncCount != 0 {
		for ; asyncCount > 0; asyncCount-- {
			res := <-asyncResult
			if res.err != nil {
				log.WithError(res.err).Errorln("テストに失敗しました")
				correctFlag = false
			}
			results = append(results, res.result)
		}
	}

	return results, correctFlag
}
