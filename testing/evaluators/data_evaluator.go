package evaluators

import (
	"module-tester/testing/parameters"
	"module-tester/testing/repository"
)

// dataEvaluator : 2つのテキストの値を比較するためのオブジェクト
type dataEvaluator struct {
	Source     parameters.Body
	Condition  parameters.Body
	Repository repository.Repository
}

// Evaluation : テキストを比較し、結果を返却する
func (d dataEvaluator) Evaluation() EvaluationResult {
	switch {
	case d.Condition.IsText():
		return CreateTextEvaluator(
			d.Source.DataToString(),
			d.Condition.DataToString(),
		).Evaluation()
	case d.Condition.IsJOSN():
		return CreateJSONEvaluator(
			d.Source.Data,
			d.Condition.Data,
			d.Repository,
		).Evaluation()
	}
	return EvaluationResult{
		Message: "'" + d.Condition.Format + "' は存在しないデータのフォーマットです",
		Correct: false,
	}
}

// CreateDataEvaluator : 2つのテキストの値を比較するオブジェクトを生成する
func CreateDataEvaluator(source parameters.Body, condition parameters.Body, repository repository.Repository) Evaluator {
	return dataEvaluator{
		Source:     source,
		Condition:  condition,
		Repository: repository,
	}
}
