package evaluators

import (
	"errors"
	"os/exec"
	"time"

	"github.com/mitchellh/go-ps"
)

// binaryEvaluator : 2つのバイナリデータを比較するためのオブジェクト
type processEvaluator struct {
	Cmd                 *exec.Cmd
	SurvivalTimeSeconds int
}

// Evaluation : 2つのバイナリデータを比較し、結果を返却する
func (h processEvaluator) Evaluation() EvaluationResult {
	if h.SurvivalTimeSeconds > 0 {
		time.Sleep(time.Second * time.Duration(h.SurvivalTimeSeconds))
	}

	info, err := ps.FindProcess(h.Cmd.Process.Pid)
	if err != nil {
		return EvaluationResult{
			Correct: false,
			Message: err.Error(),
		}
	}

	if info == nil {
		return EvaluationResult{
			Correct: false,
			Message: "プロセスが見つかりませんでした",
		}
	}

	return EvaluationResult{
		Correct: true,
		Message: CorrectMessage,
	}
}

// CreateProcessEvaluator : 2つのバイナリの値を比較するオブジェクトを生成する
func CreateProcessEvaluator(cmd *exec.Cmd, survivalTimeSeconds int) Evaluator {
	if cmd == nil {
		return CreateFailedEvaluator(errors.New("Cmdオブジェクトがnilです"))
	}

	if survivalTimeSeconds < 0 {
		return CreateFailedEvaluator(errors.New("プロセス生存時間は0以上の値が指定される必要があります"))
	}

	return processEvaluator{
		Cmd:                 cmd,
		SurvivalTimeSeconds: survivalTimeSeconds,
	}
}
