package evaluators

import (
	"module-tester/testing/parameters"
	"module-tester/testing/repository"
	"net/http"
	"strings"
)

// httpResponseEvaluator : HTTPレスポンスを比較するためのオブジェクト
type httpResponseEvaluator struct {
	Source     HTTPResponseCondition
	Condition  HTTPResponseCondition
	Repository repository.Repository
}

// httpRequestEvaluator : HTTPリクエストを比較するためのオブジェクト
type httpRequestEvaluator struct {
	Source     HTTPRequestCondition
	Condition  HTTPRequestCondition
	Repository repository.Repository
}

// HTTPResponseCondition : HTTPレスポンスの判定用データ
type HTTPResponseCondition struct {
	StatusCode int             `json:"StatusCode"`
	Header     http.Header     `json:"Header"`
	Body       parameters.Body `json:"Body"`
}

// HTTPRequestCondition : HTTPリクエストの判定用データ
type HTTPRequestCondition struct {
	SubPath string          `json:"SubPath"`
	Method  string          `json:"Method"`
	Header  http.Header     `json:"Header"`
	Body    parameters.Body `json:"Body"`
}

// Evaluation : HTTPレスポンスを比較し、結果を返却する
func (h httpResponseEvaluator) Evaluation() EvaluationResult {
	if h.Condition.StatusCode != 0 && h.Condition.StatusCode != h.Source.StatusCode {
		return EvaluationResult{
			Correct: false,
			Message: "ステータスコードが異なります",
		}
	}

	if h.Condition.Header != nil {
		for key, value := range h.Condition.Header {
			if _, exist := h.Source.Header[key]; exist {
				for _, item := range value {
					if !strings.Contains(h.Source.Header.Get(key), item) {
						return EvaluationResult{
							Correct: false,
							Message: "条件ヘッダーが含まれていません : '" + key + "' の '" + item + "'",
						}
					}
				}
			} else {
				return EvaluationResult{
					Correct: false,
					Message: "ヘッダーが不十分です : '" + key + "'",
				}
			}
		}
	}

	return CreateDataEvaluator(
		h.Source.Body,
		h.Condition.Body,
		h.Repository,
	).Evaluation()
}

// Evaluation : HTTPリクエストを比較し、結果を返却する
func (h httpRequestEvaluator) Evaluation() EvaluationResult {
	if h.Condition.Method != "" && h.Condition.Method != h.Source.Method {
		return EvaluationResult{
			Correct: false,
			Message: "HTTPメソッドが異なります",
		}
	}

	if h.Condition.SubPath != "" && h.Condition.SubPath != h.Source.SubPath {
		return EvaluationResult{
			Correct: false,
			Message: "パスが異なります",
		}
	}

	if h.Condition.Header != nil {
		for key, value := range h.Condition.Header {
			if _, exist := h.Source.Header[key]; exist {
				for _, item := range value {
					if !strings.Contains(h.Source.Header.Get(key), item) {
						return EvaluationResult{
							Correct: false,
							Message: "条件ヘッダーが含まれていません : '" + key + "' の '" + item + "'",
						}
					}
				}
			} else {
				return EvaluationResult{
					Correct: false,
					Message: "ヘッダーが不十分です : '" + key + "'",
				}
			}
		}
	}

	return CreateDataEvaluator(
		h.Source.Body,
		h.Condition.Body,
		h.Repository,
	).Evaluation()
}

// CreateHTTPResponseEvaluator : HTTPレスポンスを比較するためのオブジェクトを生成する
func CreateHTTPResponseEvaluator(source HTTPResponseCondition, condition HTTPResponseCondition, repository repository.Repository) Evaluator {
	return httpResponseEvaluator{
		Source:     source,
		Condition:  condition,
		Repository: repository,
	}
}

// CreateHTTPRequestEvaluator : HTTPリクエストを比較するためのオブジェクトを生成する
func CreateHTTPRequestEvaluator(source HTTPRequestCondition, condition HTTPRequestCondition, repository repository.Repository) Evaluator {
	return httpRequestEvaluator{
		Source:     source,
		Condition:  condition,
		Repository: repository,
	}
}
