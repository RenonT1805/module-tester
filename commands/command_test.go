package commands_test

import (
	"module-tester/commands"
	"module-tester/constants"
	"module-tester/test"
	"reflect"
	"testing"

	"github.com/sirupsen/logrus"
)

type SuccessDummyCommand struct {
}

func (d SuccessDummyCommand) CommandName() string {
	return "success-dummy"
}

func (d SuccessDummyCommand) CommandDescription() string {
	return "description"
}

func (d SuccessDummyCommand) Run([]string) constants.ResponseCode {
	return constants.Successful
}

type FailedDummy struct {
}

func (d FailedDummy) CommandName() string {
	return "failed-dummy"
}

func (d FailedDummy) CommandDescription() string {
	return "description"
}

func (d FailedDummy) Run([]string) constants.ResponseCode {
	return constants.Failed
}

func TestGetAllCommand(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		want []commands.Command
	}{
		{
			name: "すべてのコマンドが取得されるか",
			want: []commands.Command{
				&commands.Test{},
				&commands.Version{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := commands.GetAllCommands(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAllCommand() = %q, want %q", got, tt.want)
			}
		})
	}
}

func TestRunCommands(t *testing.T) {
	logrus.SetOutput(test.DummyWriter{})
	t.Parallel()
	type args struct {
		args    []string
		options commands.Options
	}
	tests := []struct {
		name string
		args args
		want constants.ResponseCode
	}{
		{
			name: "存在しないサブコマンド",
			args: args{
				args: []string{"not-exist-command"},
				options: commands.Options{
					Commands: []commands.Command{
						SuccessDummyCommand{},
					},
					Version: false,
				},
			},
			want: constants.NotFound,
		},
		{
			name: "サブコマンド指定",
			args: args{
				args: []string{"success-dummy"},
				options: commands.Options{
					Commands: []commands.Command{
						SuccessDummyCommand{},
					},
					Version: false,
				},
			},
			want: constants.Successful,
		},
		{
			name: "サブコマンドオプション指定",
			args: args{
				args: []string{"success-dummy", "test"},
				options: commands.Options{
					Commands: []commands.Command{
						SuccessDummyCommand{},
					},
					Version: false,
				},
			},
			want: constants.Successful,
		},
		{
			name: "失敗",
			args: args{
				args: []string{"failed-dummy"},
				options: commands.Options{
					Commands: []commands.Command{
						FailedDummy{},
					},
					Version: false,
				},
			},
			want: constants.Failed,
		},
		{
			name: "サブコマンド未指定",
			args: args{
				args: []string{},
				options: commands.Options{
					Commands: []commands.Command{},
					Version:  false,
				},
			},
			want: constants.Successful,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := commands.RunCommands(tt.args.args, tt.args.options); got != tt.want {
				t.Errorf("RunCommand() = %v, want %v", got, tt.want)
			}
		})
	}
}
