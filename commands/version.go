package commands

import (
	"flag"
	"fmt"
	"module-tester/constants"

	log "github.com/sirupsen/logrus"
)

// Version : バージョン表示コマンド
type Version struct {
	Flags        *flag.FlagSet
	ScenarioFile string
	Out          string
	ScenarioName string
}

// showVersion : バージョン情報を表示する
func showVersion() {
	log.Printf("version %s\n", constants.Version)
}

func (v Version) showCommandUsage() {
	out := log.StandardLogger().Out
	flag.CommandLine.SetOutput(out)
	fmt.Fprintf(out, "\n使い方: %s %s\n", constants.LowerAppName, v.CommandName())
	fmt.Fprintf(out, "\n説明: %s\n", v.CommandDescription())
}

// initialize : コマンドの初期化
func (v *Version) initialize(args []string) error {
	v.Flags = flag.NewFlagSet(v.CommandName(), flag.ExitOnError)
	v.Flags.Usage = v.showCommandUsage
	v.Flags.Parse(args)
	return nil
}

// CommandName : コマンド名を返す
func (v Version) CommandName() string {
	return "version"
}

// CommandDescription : コマンド説明を返す
func (v Version) CommandDescription() string {
	return "バージョン情報を表示します。"
}

// Run : コマンドの実行
func (v Version) Run(args []string) constants.ResponseCode {
	if err := v.initialize(args); err != nil {
		log.WithError(err).Errorln("初期化に失敗しました")
		return constants.Failed
	}

	showVersion()
	return constants.Successful
}
