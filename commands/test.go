package commands

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"module-tester/constants"
	"module-tester/testing"
	"os"

	log "github.com/sirupsen/logrus"
)

// Test : テストコマンド
type Test struct {
	Flags        *flag.FlagSet
	ScenarioFile string
	Out          string
	ScenarioName string
}

func (g Test) showCommandUsage() {
	out := log.StandardLogger().Out
	flag.CommandLine.SetOutput(out)
	fmt.Fprintf(out, "\n使い方: %s %s [options...]\n", constants.LowerAppName, g.CommandName())
	fmt.Fprintf(out, "\n説明: %s\n", g.CommandDescription())
	fmt.Fprintf(out, "\n実行オプション:\n")
	g.Flags.PrintDefaults()
}

// initialize : コマンドの初期化
func (t *Test) initialize(args []string) error {
	t.Flags = flag.NewFlagSet(t.CommandName(), flag.ExitOnError)
	t.Flags.Usage = t.showCommandUsage
	t.Flags.StringVar(&t.ScenarioFile, "scenario", "", "テストを実行するシナリオファイルを指定します（必須）")
	t.Flags.StringVar(&t.Out, "out", "", "テストの結果を出力するファイル名を指定します（指定なしの場合、結果ファイルを出力しません）")
	t.Flags.StringVar(&t.ScenarioName, "name", "", "実行するシナリオ名を指定します（指定なしまたは空の場合、すべてのシナリオを実行します）")
	t.Flags.Parse(args)

	if t.ScenarioFile == "" {
		return errors.New("scenarioの指定は必須です")
	}
	return nil
}

// CommandName : コマンド名を返す
func (t Test) CommandName() string {
	return "test"
}

// CommandDescription : コマンド説明を返す
func (t Test) CommandDescription() string {
	return "予め作成されたシナリオファイルを元に、テストを実行します。"
}

// Run : コマンドの実行
func (t Test) Run(args []string) constants.ResponseCode {
	if err := t.initialize(args); err != nil {
		log.WithError(err).Errorln("初期化に失敗しました")
		return constants.Failed
	}

	scenarioDataSource, err := ioutil.ReadFile(t.ScenarioFile)
	if err != nil {
		log.Errorln(err)
		return constants.Failed
	}

	var scenarioData testing.ScenarioTest
	err = json.Unmarshal(scenarioDataSource, &scenarioData)
	if err != nil {
		log.WithError(err).Errorln("シナリオファイルが不正です")
		return constants.Failed
	}

	if t.ScenarioName != "" {
		exist := false
		for _, item := range scenarioData.Scenarios {
			if item.Name != t.ScenarioName {
				continue
			}
			scenarioData.Scenarios = []testing.Scenario{item}
			exist = true
			break
		}
		if !exist {
			log.WithFields(log.Fields{
				"シナリオ名": t.ScenarioName,
			}).Errorln("シナリオが見つかりません")
			return constants.Failed
		}
	}

	results, correctFlag := testing.RunScenarioTest(scenarioData)

	if t.Out != "" {
		resultContent, err := json.Marshal(results)
		if err != nil {
			log.WithError(err).Errorln("テスト結果が不正です")
			return constants.Failed
		}

		var buf bytes.Buffer
		err = json.Indent(&buf, resultContent, "", "  ")
		if err != nil {
			log.Errorln(err)
		}

		err = ioutil.WriteFile(t.Out, buf.Bytes(), os.ModePerm)
		if err != nil {
			log.Errorln(err)
			return constants.Failed
		}

		log.WithField("出力ファイル", t.Out).Infoln("テスト結果ファイルを出力しました")
	}

	if correctFlag {
		log.Infoln("すべてのテストが正常に終了しました")
		return constants.Successful
	}

	log.Errorln("失敗したテストが存在します")
	return constants.Failed
}
