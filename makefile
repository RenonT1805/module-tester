build:
	go vet
	go build

test-code:
	go test -v ./... -cover

run-sample:
	make build
	module-tester.exe test --scenario=sample.json --out=result.json