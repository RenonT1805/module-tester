package constants

// AppName : このアプリケーションの名前
const AppName = "ModuleTester"

// LowerAppName : 小文字にしたこのアプリケーションの名前
const LowerAppName = "module-tester"

// Version : このアプリケーションのバージョン
const Version = "v0.2.1"
